package WekaTree;

import weka.classifiers.trees.J48;
import weka.core.Instances;

public class InstancesClassifier {
    private final Instances instances;
    public InstancesClassifier(Instances instances) {
        this.instances = instances;
    }

public Instances classify() {
        J48 j = loadModel();
        Instances labeled = new Instances(instances);
        try { for (int i = 0; i < instances.numInstances(); i++) {
            double label = j.classifyInstance(labeled.instance(i));
            labeled.instance(i).setClassValue(label);}}
        catch (Exception e) {
            System.err.println("Unable to classify instances, please try again!");
            System.exit(0);
        }
        return labeled;
    }

private J48 loadModel() {
        try {
            return (J48) weka.core.SerializationHelper.read("/Users/Jakko/Desktop/WekaTree/data/j48.model");
        } catch (Exception e) {
            System.err.println("Unable to Load model, please try again!");
            System.exit(0);
        }
        return null;
    }
}
