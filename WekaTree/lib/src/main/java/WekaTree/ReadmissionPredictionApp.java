package WekaTree;
/**
 * Main class
 *
 */
public class ReadmissionPredictionApp {

    /**
     * @param args The command line arguments
     */
    public static void main(String[] args) {
        CommandLineOptions processor = new CommandLineOptions(args);
        processor.start();
    }
}


